package gemini_test

import (
	"crypto/tls"
	"fmt"
	"log"
	"net/url"
	"os"
	"time"

	"codeberg.org/FiskFan1999/gemini"
)

func handler(u *url.URL, c *tls.Conn) gemini.Response {
	return gemini.ResponseFormat{
		gemini.Success, "text/gemini", gemini.Lines{
			fmt.Sprintf("%sHello!", gemini.Header),
			"Welcome to my Gemini capsule!",
		},
	}
}

func ExampleServer() {
	// Load certificates
	cert, err := os.ReadFile("cert.pem")
	if err != nil {
		log.Fatal(err.Error())
	}
	key, err := os.ReadFile("key.pem")
	if err != nil {
		log.Fatal(err.Error())
	}

	// initialize and run server
	serv := gemini.GetServer(":1965", cert, key)
	serv.Handler = handler
	go func() {
		time.Sleep(time.Second)
		serv.Shutdown <- 0
	}()
	if err := serv.Run(); err != nil {
		panic(err)
	}
}

package gemini

import (
	"bytes"
	"errors"
	"fmt"
	"net"
	"net/url"
	"testing"
	"time"
)

func TestClientLogger(t *testing.T) {
	var buf bytes.Buffer
	cli := Client{
		Dialer:      &net.Dialer{Timeout: time.Second * 5},
		ReadSize:    1024 * 1024 * 16, // 16mB
		ReadTimeout: time.Second * 5,
		Logger:      &buf,
	}
	u, _ := url.Parse("gemini://gemini.circumlunar.space")
	cli.Get(u)
	t.Logf("\n%s", buf.String())
}

func TestMedusae(t *testing.T) {
	/*
		This capsule seems to be giving the client trouble
	*/

	cli := Client{
		Dialer:      &net.Dialer{Timeout: time.Second * 5},
		ReadSize:    1024 * 1024 * 16, // 16mB
		ReadTimeout: time.Second * 5,
	}
	u, _ := url.Parse("gemini://medusae.space/")
	resp, err := cli.Get(u)
	t.Log(string(resp), err)
	if err != nil && err.Error() != "dial tcp: lookup medusae.space: no such host" {
		t.Error("error recieved")
	}
}

func TestAggr(t *testing.T) {
	cli := Client{
		Dialer:      &net.Dialer{Timeout: time.Second * 5},
		ReadSize:    1024 * 1024 * 16, // 16mB
		ReadTimeout: time.Second * 5,
	}
	u, _ := url.Parse("gemini://calcuode.com/gmisub-aggregate.gmi")
	resp, err := cli.Get(u)
	t.Log(string(resp), err)
	if err != nil {
		if err.Error() == "dial tcp: lookup calcuode.com: no such host" {
			t.Skip("No such host. Skipping")
		}

	}
	if len(resp) == 0 {
		t.Error("Zero length response")
	}
}

func ExampleParseResponse() {
	status, mime, err := ParseResponse([]byte("20 text/gemini\r\nHello!"))
	if err != nil {
		fmt.Println(err.Error())
	}
	fmt.Printf("status=%d\n", status)
	fmt.Printf("mime=%q\n", mime)
	// Output: status=20
	// mime="text/gemini"
}

func TestTimeout(t *testing.T) {
	cli := Client{
		Dialer:      &net.Dialer{Timeout: time.Microsecond},
		ReadSize:    1024 * 1024 * 16, // 16mB
		ReadTimeout: time.Second,
	}
	u, _ := url.Parse("gemini://skyjake.fi/")
	resp, err := cli.Get(u)
	t.Log(resp, err)
	if !errors.Is(err, ErrTimeout) {
		if err != nil && err.Error() == "dial tcp: lookup skyjake.fi: no such host" {
			t.Skip("No such host. Skipping")
		}
		t.Fatalf("ErrTimeout not recieved, recieved %s", err)
	}

	cli2 := Client{
		Dialer:      &net.Dialer{Timeout: time.Second * 2},
		ReadSize:    1024 * 1024 * 16, // 16mB
		ReadTimeout: time.Nanosecond,
	}
	u2, _ := url.Parse("gemini://gemini.conman.org/")
	resp, err = cli2.Get(u2)
	t.Log(resp, err)
	if !errors.Is(err, ErrTimeout) {
		if err != nil && err.Error() == "dial tcp: lookup gemini.conman.org: no such host" {
			t.Skip("No such host. Skipping")
		}
		t.Fatalf("ErrTimeout not recieved, recieved %s", err)
	}
}

func ExampleClient() {
	d := Client{
		Dialer:      &net.Dialer{Timeout: time.Second},
		ReadSize:    1024 * 1024 * 16, // 16mB
		ReadTimeout: time.Second,
	}

	u, err := url.Parse("gemini://gemini.circumlunar.space/")
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	if _, err := d.Get(u); err != nil {
		fmt.Println(err.Error())
		return
	}
}

func TestError(t *testing.T) {
	var cases = map[string]error{
		"gemini://gemini.circumlunar.space/": nil,
		"https://gemini.circumlunar.space/":  ErrWrongProtocol,
	}

	for ustr, expectedErr := range cases {
		u, err := url.Parse(ustr)
		if err != nil {
			t.Errorf("%s - %s", ustr, err)
		}
		t.Logf("%#v", u)
		_, err = Client{Dialer: &net.Dialer{Timeout: time.Second}, ReadSize: 1024 * 1024 * 16, ReadTimeout: time.Second}.Get(u)
		if !errors.Is(expectedErr, err) {
			if err != nil && err.Error() == "dial tcp: lookup gemini.circumlunar.space: no such host" {
				t.Skip("No such host. Skipping")
			}
			t.Errorf("%s - expected %s, recieved %s", ustr, expectedErr, err)
		}
	}
}

func TestOverflow(t *testing.T) {
	d := Client{
		Dialer:      &net.Dialer{Timeout: time.Second},
		ReadSize:    16, // 16mB
		ReadTimeout: time.Second,
	}

	u, err := url.Parse("gemini://geminispace.info/")
	if err != nil {
		t.Fatal(err.Error())
	}
	response, err := d.Get(u)
	if !errors.Is(err, ErrResponseTooLarge) {
		if err != nil && err.Error() == "dial tcp: lookup geminispace.info: no such host" {
			t.Skip("No such host. Skipping")
		}
		t.Fatalf("Recieved wrong error: %q", err.Error())
	}
	if response != nil {
		t.Fatalf("Byte slice is not nil, recieved %q", response)
	}
}

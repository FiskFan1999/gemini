package gemini

import (
	"net/url"
	"testing"
)

func TestNormalizeURI(t *testing.T) {
	for test, expected := range TestnormalizeURICases {
		u, err := url.Parse(test)
		if err != nil {
			t.Fatal(err.Error())
		}

		normalizeURI(u) // modifies url
		result := u.String()
		if result != expected {
			t.Errorf("For %s, \texpected %s \t but recieved %s", test, expected, result)
		}
	}
}

var TestnormalizeURICases map[string]string = map[string]string{
	"gemini://example.net/":                       "gemini://example.net/",                  // completely valid url
	"gemini://example.net":                        "gemini://example.net/",                  // add trailing slash (empty path)
	"gemini://example.net/hello":                  "gemini://example.net/hello/",            // add trailing slash to end of path
	"gemini://example.net/hello//there/":          "gemini://example.net/hello/there/",      // remove double slash
	"gemini://example.net/hello/../there":         "gemini://example.net/there/",            // completely valid url
	"gemini://example.net/hello/./there":          "gemini://example.net/hello/there/",      // single dot refers to same directory not parent
	"gemini://example.net/hello/there/my/../name": "gemini://example.net/hello/there/name/", // completely valid url
}

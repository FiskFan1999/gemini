package gemini

import (
	"bytes"
	"fmt"
)

func ExampleLines() {
	var l Lines
	l.Line("1")
	l.Line("2", "3")
	l.Link("example.net")
	l.LinkDesc("example.net", "a website")
	l.Pre("Hello.", "This is a pre-formatted block.", "Goodbye.")
	l.Header(0, "header")
	l.Header(1, "header")
	l.Header(2, "header")
	l.Header(3, "header")
	l.Header(4, "header")
	l.UL("first", "second", "third")
	l.Quote("Hello.\nThis is a quote.\r\n Goodbye!")

	response := ResponseFormat{Status: Success, Mime: "text/gemini", Lines: l}

	fmt.Printf("%s", bytes.ReplaceAll(response.Bytes(), []byte("\r\n"), []byte("\n")))
	// Output:
	// 20 text/gemini
	// 1
	// 2
	// 3
	// => example.net
	// => example.net a website
	// ```
	// Hello.
	// This is a pre-formatted block.
	// Goodbye.
	// ```
	// # header
	// # header
	// ## header
	// ### header
	// # header
	// * first
	// * second
	// * third
	// > Hello.
	// > This is a quote.
	// > Goodbye!
}

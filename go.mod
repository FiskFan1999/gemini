module codeberg.org/FiskFan1999/gemini

go 1.19

require github.com/madflojo/testcerts v1.0.1

require (
	github.com/google/go-cmp v0.5.9 // indirect
	github.com/sergi/go-diff v1.0.0 // indirect
	golang.org/x/mod v0.6.0-dev.0.20220419223038-86c51ed26bb4 // indirect
	golang.org/x/sys v0.0.0-20221010160319-abe0a0adba9c // indirect
	golang.org/x/tools v0.1.12 // indirect
	gopkg.in/src-d/go-git.v4 v4.13.1 // indirect
)

package gemini

import (
	"bytes"
	"crypto/tls"
	"fmt"
	"io"
	"net"
	"net/url"
	"os"
	"strings"
	"testing"
	"time"

	"github.com/madflojo/testcerts"
)

func TestServerLogger(t *testing.T) {
	log := new(bytes.Buffer)

	cert, key, err := testcerts.GenerateCerts()
	if err != nil {
		t.Fatal(err.Error())
	}

	serv := GetServer("127.0.0.1:0", cert, key)
	serv.Logger = log
	serv.Handler = func(u *url.URL, t *tls.Conn) Response {
		return ResponseFormat{Success, "text/gemini", nil}
	}
	go serv.Run()
	<-serv.Ready // wait until server is ready (new address will be assigned.)
	defer func() {
		serv.Shutdown <- 0
		<-serv.ShutdownCompleted
	}()

	d := Client{
		Dialer:      &net.Dialer{Timeout: time.Second},
		ReadSize:    1024 * 1024 * 16, // 16mB
		ReadTimeout: time.Second,
	}

	u, err := url.Parse(fmt.Sprintf("gemini://%s/", serv.Address))
	if err != nil {
		t.Fatal(err.Error())
	}

	d.Get(u)

	u2, err := url.Parse(fmt.Sprintf("gemini://%s/hello/", serv.Address))
	if err != nil {
		t.Fatal(err.Error())
	}

	d.Get(u2)

	logbytes := log.Bytes()
	t.Logf("\n%s", logbytes)

	if len(bytes.Split(logbytes, []byte("\n"))) != 3 {
		t.Fatalf("Recieved: %q", logbytes)
	}
}

func TestResponseRead(t *testing.T) {
	file, err := os.CreateTemp("", "*.txt")
	if err != nil {
		t.Fatal(err.Error())
	}
	fmt.Fprint(file, "Hello.")
	file.Close()
	defer os.Remove(file.Name())
	handler := func(u *url.URL, c *tls.Conn) Response {
		file2, err := os.Open(file.Name()) // is closed by the response
		if err != nil {
			t.Fatal(err.Error())
		}
		return ResponseRead{Content: file2, Mime: "text/plain", Name: "text.txt"}
	}

	resp := handler(nil, nil)
	output := resp.Bytes()
	expected := []byte("20 text/plain\r\nHello.")
	if !bytes.Equal(output, expected) {
		t.Errorf("Expected output %q but recieved %q", expected, output)
	}

}

func TestString(t *testing.T) {
	t.Log(ResponseFormat{
		Status: Success,
		Mime:   "text/gemini",
		Lines: Lines{
			"hello",
		},
	})
	t.Log(ResponsePlain("20 text/gemini\r\nhello again.\r\n"))
}

func FuzzTestInput(f *testing.F) {

	f.Add("", "0")
	f.Add(strings.Repeat("a", 716), strings.Repeat("hi", 1024))

	cert, key, err := testcerts.GenerateCerts()
	if err != nil {
		f.Fatal(err.Error())
	}

	serv := GetServer("127.0.0.1:0", cert, key)
	serv.Handler = func(u *url.URL, t *tls.Conn) Response {
		parts := strings.FieldsFunc(u.EscapedPath(), func(r rune) bool { return r == '/' })
		var rpath string
		if len(parts) > 1 {
			return ResponseFormat{
				Status: BadRequest,
				Mime:   "different parts len",
				Lines:  nil,
			}
		} else if len(parts) == 1 {
			rpath, err = url.PathUnescape(parts[0])
			if err != nil {
				return ResponseFormat{
					Status: BadRequest,
					Mime:   err.Error(),
					Lines:  nil,
				}
			}
		}
		rquery, err := url.QueryUnescape(u.RawQuery)
		if err != nil {
			return ResponseFormat{
				Status: BadRequest,
				Mime:   err.Error(),
				Lines:  nil,
			}
		}
		return ResponseFormat{
			Status: Success,
			Mime:   "text/gemini",
			Lines: Lines{
				fmt.Sprintf("Path=%s", rpath),
				fmt.Sprintf("Quer=%s", rquery),
			},
		}
	}
	go serv.Run()
	<-serv.Ready // wait until server is ready (new address will be assigned.)
	defer func() {
		serv.Shutdown <- 0
		<-serv.ShutdownCompleted
	}()

	f.Fuzz(func(t *testing.T, path string, query string) {
		newUrl, err := url.Parse(fmt.Sprintf("/%s/?%s", url.PathEscape(path), url.QueryEscape(query)))
		if err != nil {
			t.Fatal(err.Error())
		}
		expectedOutput := fmt.Sprintf("20 text/gemini\r\nPath=%s\r\nQuer=%s\r\n", path, query)

		// connection
		conn, err := tls.Dial("tcp", serv.Address, &tls.Config{
			InsecureSkipVerify: true,
		})
		if err != nil {
			t.Fatal(err.Error())
		}
		defer conn.Close()

		fmt.Fprintf(conn, "%s\r\n", newUrl)

		outputBytes, err := io.ReadAll(conn)
		if err != nil {
			t.Fatal(err.Error())
		}

		if len(newUrl.String()) <= 1024 {
			if bytes.Compare(outputBytes, []byte(expectedOutput)) != 0 && !(path == "." || path == "..") {
				t.Error("Result does not match expectation.")
			}
		} else {
			if bytes.Compare(outputBytes, []byte("59 Request too long\r\n")) != 0 {
				t.Error("Result does not match expectation for too long input.")
			}
		}
	})
}

func TestRawResponse(t *testing.T) {
	cert, key, err := testcerts.GenerateCerts()
	if err != nil {
		t.Fatal(err.Error())
	}

	serv := GetServer("127.0.0.1:0", cert, key)
	serv.Handler = func(u *url.URL, t *tls.Conn) Response {
		return ResponsePlain(TestRawResponseText)
	}
	go serv.Run()
	<-serv.Ready // wait until server is ready (new address will be assigned.)
	defer func() {
		serv.Shutdown <- 0
		<-serv.ShutdownCompleted
	}()
	conn, err := tls.Dial("tcp", serv.Address, &tls.Config{
		InsecureSkipVerify: true,
	})
	if err != nil {
		t.Fatal(err.Error())
	}
	defer conn.Close()

	fmt.Fprint(conn, "/\r\n")

	outputBytes, err := io.ReadAll(conn)
	if err != nil {
		t.Fatal(err.Error())
	}

	if bytes.Compare(outputBytes, TestRawResponseText) != 0 {
		t.Error("Result does not match expectation.")
	}
}

var TestRawResponseText = []byte("20 text/plain\r\nHello, this is william\nBye.")

func TestConstructResponse(t *testing.T) {
	testConstructResponseCases := []TestConstructResponseCasesStr{
		{
			Success,
			"text/gemini",
			Lines{fmt.Sprintf("%sHello!", Header), "", "This is the first page."},
			[]byte(fmt.Sprintf("20 text/gemini\r\n%sHello!\r\n\r\nThis is the first page.\r\n", Header)),
		},
	}

	for i, c := range testConstructResponseCases {
		var output []byte
		output = ResponseFormat{c.Status, c.Mime, c.Lines}.Bytes()
		if bytes.Compare(output, c.Result) != 0 {
			// not equal. problem
			t.Errorf("Test %d - Expected result did not match.\nExpected\n%s\nRecieved\n%s", i, c.Result, output)
		}
	}
}

var TestReadLimitCases = map[int64]string{
	3:    "20 text/gemini\r\n",
	128:  "20 text/gemini\r\n",
	256:  "20 text/gemini\r\n",
	512:  "20 text/gemini\r\n",
	1023: "20 text/gemini\r\n",
	1024: "20 text/gemini\r\n",
	1025: "59 Request too long\r\n",
	2000: "59 Request too long\r\n",
}

func TestReadLimit(t *testing.T) {
	cert, key, err := testcerts.GenerateCerts()
	if err != nil {
		t.Fatal(err.Error())
	}

	serv := GetServer("127.0.0.1:0", cert, key)
	serv.Handler = func(u *url.URL, t *tls.Conn) Response {
		return ResponseFormat{20, "text/gemini", nil}
	}

	go serv.Run()
	<-serv.Ready // wait until server is ready (new address will be assigned.)
	t.Logf("new address %s", serv.Address)
	defer func() {
		serv.Shutdown <- 0
		<-serv.ShutdownCompleted
	}()

	for l, expected := range TestReadLimitCases {
		url := fmt.Sprintf("/%s/", strings.Repeat("a", int(l)-2))
		conn, err := tls.Dial("tcp", serv.Address, &tls.Config{
			InsecureSkipVerify: true,
		})
		if err != nil {
			t.Fatal(err.Error())
		}
		defer conn.Close()

		go fmt.Fprintf(conn, "%s\r\n", url)
		outputBytes, err := io.ReadAll(conn)
		if err != nil {
			t.Fatal(err.Error())
		}
		if !bytes.Equal(outputBytes, []byte(expected)) {
			t.Errorf("For %d long path, expected status %s but recieved %s", l, expected, outputBytes)
		}
	}
}

func TestGeminiServer(t *testing.T) {
	cert, key, err := testcerts.GenerateCerts()
	if err != nil {
		t.Fatal(err.Error())
	}

	serv := GetServer("127.0.0.1:0", cert, key)
	serv.Handler = func(u *url.URL, t *tls.Conn) Response {
		return ResponseFormat{20, "text/gemini", Lines{
			"Hello", "Welcome",
		}}
	}
	expected := []byte("20 text/gemini\r\nHello\r\nWelcome\r\n")

	go serv.Run()
	<-serv.Ready // wait until server is ready (new address will be assigned.)
	t.Logf("new address %s", serv.Address)
	defer func() {
		serv.Shutdown <- 0
		<-serv.ShutdownCompleted
	}()

	conn, err := tls.Dial("tcp", serv.Address, &tls.Config{
		InsecureSkipVerify: true,
	})
	if err != nil {
		t.Fatal(err.Error())
	}
	defer conn.Close()

	fmt.Fprint(conn, "/\r\n")

	outputBytes, err := io.ReadAll(conn)
	if err != nil {
		t.Fatal(err.Error())
	}

	if bytes.Compare(outputBytes, expected) != 0 {
		t.Error("Result does not match expectation.")
	}

}

type TestConstructResponseCasesStr struct {
	Status
	Mime string
	Lines
	Result []byte
}

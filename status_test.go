package gemini

import (
	"errors"
	"fmt"
	"testing"
)

func TestErrorNil(t *testing.T) {
	PermanentFailure.Error(nil)
	// will pass if not panic
}

func ExampleStatus_Response() {
	resp := RedirectPermanent.Response("/")
	fmt.Printf("%q\n", resp.Bytes())
	// Output: "31 /\r\n"
}

func ExampleStatus_Error() {
	err := errors.New("Internal server error")
	resp := TemporaryFailure.Error(err)
	fmt.Printf("%q\n", resp.Bytes())
	// Output: "40 Internal server error\r\n"
}

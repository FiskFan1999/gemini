package gemtest_test

import (
	"crypto/tls"
	"fmt"
	"net/url"

	"codeberg.org/FiskFan1999/gemini"
	"codeberg.org/FiskFan1999/gemini/gemtest"
)

func handler(u *url.URL, c *tls.Conn) gemini.Response {
	return gemini.ResponseFormat{
		gemini.Success,
		"text/gemini",
		gemini.Lines{
			"Hello!",
		},
	}
}

func ExampleCheck() {
	err, diff := gemtest.Check(handler, "/", []byte("20 text/gemini\r\nHello!\r\n"))
	fmt.Println(err, diff)
	// Output: <nil>
}

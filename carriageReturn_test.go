package gemini

import (
	"bytes"
	"testing"
)

func TestInsertCarriageReturn(t *testing.T) {
	for in, expected := range TestInsertCarriageReturnCases {
		out := InsertCarriageReturn([]byte(in))
		if !bytes.Equal(out, expected) {
			t.Errorf("%q - expected %q recieved %q", in, expected, out)
		} else {
			t.Logf("%q -> %q", in, out)
		}
	}
}

var TestInsertCarriageReturnCases = map[string][]byte{
	"20 text/gemini\r\nhello world!":     []byte("20 text/gemini\r\nhello world!"),
	"\n20 text/gemini\r\nhello world!":   []byte("\r\n20 text/gemini\r\nhello world!"),
	"\r\n20 text/gemini\r\nhello world!": []byte("\r\n20 text/gemini\r\nhello world!"),
	"20 text/gemini\nhello world!":       []byte("20 text/gemini\r\nhello world!"),
}

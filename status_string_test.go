package gemini

import (
	"testing"
)

func TestStatusString(t *testing.T) {
	for a, b := range TestStatusStringCases {
		if a.String() != b {
			t.Errorf("Error, (%d) %s != b", a, a.String())
		}
	}
}

var TestStatusStringCases = map[Status]string{
	Input:                     "Input",
	SensitiveInput:            "SensitiveInput",
	Success:                   "Success",
	RedirectTemporary:         "RedirectTemporary",
	RedirectPermanent:         "RedirectPermanent",
	TemporaryFailure:          "TemporaryFailure",
	ServerUnavailable:         "ServerUnavailable",
	CGIError:                  "CGIError",
	ProxyError:                "ProxyError",
	SlowDown:                  "SlowDown",
	PermanentFailure:          "PermanentFailure",
	NotFound:                  "NotFound",
	Gone:                      "Gone",
	ProxyRequestRefused:       "ProxyRequestRefused",
	BadRequest:                "BadRequest",
	ClientCertificateRequired: "ClientCertificateRequired",
	CertificateNotAuthorised:  "CertificateNotAuthorised",
	CertificateNotValid:       "CertificateNotValid",
	255:                       "Status(255)",
}

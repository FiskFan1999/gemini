package gemtest

import (
	"crypto/sha256"
	"crypto/tls"
	"crypto/x509"
	"encoding/base64"
	"errors"
	"fmt"
	"net/url"
	"testing"

	"codeberg.org/FiskFan1999/gemini"
)

func TestCheckOutput(t *testing.T) {
	cases := []struct {
		Handler gemini.Handler
		Url     string
		Expect  []byte
		Err     error
		CO      string
	}{
		{
			func(u *url.URL, c *tls.Conn) gemini.Response {
				return gemini.ResponseFormat{
					gemini.Success,
					"text/gemini",
					gemini.Lines{
						"hello!",
					},
				}
			},
			"/",
			[]byte(""),
			ErrCheckDidNotMatch,
			"\x1b[32m20 text/gemini\r\nhello!\r\n\x1b[0m",
		},
		{
			func(u *url.URL, c *tls.Conn) gemini.Response {
				return gemini.ResponseFormat{
					gemini.Success,
					"text/gemini",
					gemini.Lines{
						"hello!",
					},
				}
			},
			"/",
			[]byte("20 text/gemini\r\nhello!\r\n"),
			nil,
			"",
		},
	}

	for i, c := range cases {
		err, difference := Check(c.Handler, c.Url, c.Expect)
		if !errors.Is(err, c.Err) {
			t.Errorf("%d - Incorrect error recieved: expected \"%s\", recieved \"%s\"", i, c.Err, err)
		}
		if c.CO != difference {
			t.Errorf("%d - Incorrect difference recieved: expected %q recieved %q", i, c.CO, difference)
		}
	}
}

func TestTestd(t *testing.T) {
	handler := func(*url.URL, *tls.Conn) gemini.Response {
		return gemini.ResponseFormat{gemini.Success, "text/plain", gemini.Lines{"hello"}}
	}
	serv := Testd(t, handler, 4)
	defer serv.Stop()
	serv.Check(Input{"/", 0, []byte("20 text/plain\r\nhello\r\n")}, Input{"/", 0, []byte("20 text/plain\r\nhello\r\n")})
	serv.Check(Input{"/", 0, []byte("20 text/plain\r\nhello\r\n")})
}

/*
From go.step.sm/crypto/x509util.Fingerprint()
*/
func certFP(cert *x509.Certificate) []byte {
	sum := sha256.Sum256(cert.Raw)
	out := make([]byte, base64.StdEncoding.EncodedLen(sha256.Size))
	base64.StdEncoding.Encode(out, sum[:])
	return out
}

func certHand(u *url.URL, c *tls.Conn) gemini.Response {
	cert := c.ConnectionState().PeerCertificates
	if len(cert) == 0 {
		return gemini.ResponseFormat{gemini.Success, "text/gemini", gemini.Lines{"no certificate"}}
	}
	fp := certFP(cert[0])
	return gemini.ResponseFormat{gemini.Success, "text/gemini", gemini.Lines{fmt.Sprintf("fingerprint: %s", fp)}}
}

var TestCertCases = []Input{
	Input{"/", 0, []byte("20 text/gemini\r\n")},
	Input{"/", 1, []byte("20 text/gemini\r\n")},
	Input{"/", 2, []byte("20 text/gemini\r\n")},
	Input{"/", 3, []byte("20 text/gemini\r\n")},
}

func TestCert(t *testing.T) {
	// note: this test will fail, but it proves that
	// the certificates are different from each
	// other
	/*
		serv := Testd(t, certHand, 3)
		defer serv.Stop()
		serv.Check(TestCertCases...)
	*/
}
